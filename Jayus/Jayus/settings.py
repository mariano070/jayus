# -*- coding: utf-8 -*-
"""
Django settings for Jayus project.

Generated by 'django-admin startproject' using Django 1.8.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '^v!)j2tqh#0mn0t$ta&kceg^#hb06t3=0(^&hd4x+qwfa-6=(q'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
# always before django.contrib.admin. It is the custom template for admin
# pisa el admin
    'bootstrap_admin',
    'django.contrib.admin',
    'JayusApp',
# es un rich text area
    'ckeditor',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'Jayus.urls'

# For Sidebar Menu (List of apps and models) (RECOMMENDED)
from django.conf import global_settings
TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'django.core.context_processors.request',
)
BOOTSTRAP_ADMIN_SIDEBAR_MENU = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

TEMPLATE_DIRS = (os.path.join(BASE_DIR, 'myAdmin'),)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)


WSGI_APPLICATION = 'Jayus.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Se usan para precargar datos
FIXTURE_DIRS = (
   os.path.join(BASE_DIR, 'Jayus/JayusApp/migrations/Fixtures'),
)
# TODO: Poner postgrl (?)


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'es-AR'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'
#PROJECT_DIR = '/home/ubuntu/Escritorio/ingweb/Jayus/JayusApp'
STATIC_ROOT = os.path.join(BASE_DIR, 'Jayus/JayusApp/static')

# para el ckeditor para el rich text
CKEDITOR_UPLOAD_PATH = "uploads/"

# Esto es el modelo del usuario custom
AUTH_USER_MODEL = 'JayusApp.CustomUser'

# Esto es para el mail.
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp-mail.outlook.com'
EMAIL_HOST_USER = 'JayusApp@hotmail.com'
EMAIL_HOST_PASSWORD = 'P4s5W0rdJayus'
EMAIL_PORT = 25
DEFAULT_FROM_EMAIL = 'JayusApp@hotmail.com'

#Días de validez del link para resetear pass
PASSWORD_RESET_TIMEOUT_DAYS = 1

#url de la pantalla de logout
#LOGOUT_URL =
