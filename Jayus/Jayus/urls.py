from django.conf.urls import include, url
from django.contrib import admin

admin.site.index_template = 'admin/my_custom_index.html'
admin.site.app_index_template = 'admin/my_custom_app_index.html'
admin.autodiscover()
#from JayusApp.admin import admin_site

urlpatterns = [
    # Examples:
    # url(r'^blog/', include('blog.urls')),

    # admin
    #url(r'^myadmin/', include(admin_site.urls)),
    url(r'^admin/', include(admin.site.urls)),
    # rich textbox
    url(r'^ckeditor/', include('ckeditor.urls')),

    # urls al home
    url(r'^$', 'JayusApp.views.home', name='home'),
    url(r'^/', 'JayusApp.views.home'),
    url(r'^home/', 'JayusApp.views.home'),

    # registrar/validar
    url(r'^registrar/', 'JayusApp.views.registrar'),
    url(r'^validar/(?P<id>[^/]+)', 'JayusApp.views.validar'),
    url(r'^validar/', 'JayusApp.views.validar'),

    # funcionales
    url(r'^perfil/(?P<idPerfil>[^/]+)', 'JayusApp.views.perfilView'),
    url(r'^perfil/', 'JayusApp.views.perfilView'),
    url(r'^comunidad/(?P<idComunidad>[^/]+)', 'JayusApp.views.comunidad'),
    url(r'^mis_comunidades/', 'JayusApp.views.mis_comunidades'),

    # statics
    url(r'^aboutUs/', 'JayusApp.views.aboutUs'),
    url(r'^gracias/', 'JayusApp.views.gracias'),

    # contenido
    url(r'^contenido/(?P<idContenido>[0-9]+)', 'JayusApp.views.contenido'),
    url(r'^contenido/', 'JayusApp.views.contenido'),
    url(r'^contenido_add/(?P<lugar>\w+)/(?P<idLugar>\w+)', 'JayusApp.views.contenido_add'),
    url(r'^contenido_add/', 'JayusApp.views.contenido_add'),
    url(r'^alta_comentario/(?P<idContenido>[0-9]+)', 'JayusApp.views.alta_comentario'),
]

#http://127.0.0.1:8000/validar?id=df7e4f22-f0fe-11e4-bb07-000c293f080b
#http://127.0.0.1:8000/contenido?idcontenido=1

