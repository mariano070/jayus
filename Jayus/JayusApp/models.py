# -*- coding: utf-8 -*-
from django.db import models
import uuid
from django.utils import timezone
from django.contrib.auth.models import Group
#from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from JayusApp.Common.Enum import SexoEnum, VisibilidadEnum, TipoContenidoEnum
from JayusApp.Common.Enum import EstadoContenidoEnum, EstadoMiembroEnum
from JayusApp.Common.Enum import RolMiembroEnum
# TODO: https://docs.djangoproject.com/en/1.8/topics/auth/default/#topic-authorization


class MyUserManager(BaseUserManager):
    def create_user(self, username, email, password):
        if not username:
            raise ValueError('Se requiere nombre de usuario')

        if not email:
            raise ValueError('Se requiere email')

        if not password:
            raise ValueError('Se requiere contraseña')

        user = self.model(
            username=username,
            email=self.normalize_email(email)
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(username, email,
            password=password
        )
        user.is_active = True
        user.is_admin = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser):
    username = models.CharField(max_length=50,
        verbose_name='Nombre de usuario',
        unique=True)
    email = models.EmailField(
        verbose_name='Correo electrónico',
        max_length=100
    )
    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    groups = models.ForeignKey(Group)

    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def get_full_name(self):
        # The user is identified by their email address
        return self.username

    def get_short_name(self):
        # The user is identified by their email address
        return self.username

    def __str__(self):              # __unicode__ on Python 2
        return self.username

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

# Create your models here.


class RegistrosPendientes(models.Model):
    token = models.CharField(max_length=64, verbose_name="Activation key",
                                default=uuid.uuid1)
    usuario_modificacion = models.ForeignKey(CustomUser, null=True,
                                            blank=True)
    fecha_generacion = models.DateField(default=timezone.now)
    es_valido = models.BooleanField(default=True)


class auditoriaField(models.Model):
    fecha_modificacion = models.DateField(default=timezone.now)
    usuario_modificacion = models.CharField(max_length=100, null=True,
                                            blank=True)
    # Está clase usa herencia para las tablas que necesitan auditoria.
    # No construye ninguna clase, pero las que la heredan tienen estos campos

    class Meta:
        abstract = True


class Perfil(models.Model):
    usuario = models.OneToOneField(CustomUser)
    imagen = models.ImageField(null=False,
                    default="http://localhost:8000/static/img/users.png")
    nombre = models.CharField(max_length=100, null=True, blank=True)
    apellido = models.CharField(max_length=100, null=True, blank=True)
    fechaNacimiento = models.DateField(default=timezone.now)
    sexo = models.CharField(max_length=1, choices=SexoEnum.choices(),
                        null=True, blank=True)
    localidad = models.CharField(blank=True, max_length=30)
    provincia = models.CharField(blank=True, max_length=30)
    pais = models.CharField(blank=True, max_length=30)
    telefono = models.CharField(blank=True, max_length=30)

    def __str__(self):
        # Ver si no hay nombre/apellido entonces user name
        return (self.usuario.username)

    def save(self, *args, **kwargs):
        if not self.usuario:
            raise ValueError('Se requiere usuario')

        # Saving the object with the default save() function
        super(Perfil, self).save(*args, **kwargs)


class Comunidad(models.Model):
    nombre = models.CharField(max_length=80, default="Test")
    descripcion = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.nombre


class Miembro(auditoriaField):
    comunidad = models.ForeignKey(Comunidad, null=False, blank=False)
    usuario = models.ForeignKey(CustomUser, null=False, blank=False)
    rol = models.CharField(max_length=1, choices=RolMiembroEnum.choices(),
                                default=RolMiembroEnum.Publicador.value)
    estado = models.CharField(max_length=1, choices=EstadoMiembroEnum.choices(),
                                default=EstadoMiembroEnum.Activo.value)

    def __str__(self):
        return ""


class Contenido(auditoriaField):
    titulo = models.CharField(max_length=100)
    contenido = RichTextField()
    estado = models.CharField(max_length=1,
                            choices=EstadoContenidoEnum.choices(),
                            default=EstadoContenidoEnum.Activo.value)
    visibilidad = models.CharField(max_length=1,
                            choices=VisibilidadEnum.choices(),
                            default=VisibilidadEnum.Privado.value)
    comunidad = models.ForeignKey(Comunidad, null=True, blank=True)
    usuario = models.ForeignKey(CustomUser, null=True, blank=True,
                related_name='usuario_creador_contenido')


class DetalleContenido(auditoriaField):
    contenido = models.ForeignKey(Contenido, null=False, blank=False)
    descripcion = models.CharField(max_length=250)
    usuario = models.ForeignKey(CustomUser, null=False,
                related_name='usuario_creador_detalle')
    tipo_contenido = models.CharField(max_length=1,
                                    choices=TipoContenidoEnum.choices(),
                                    default=TipoContenidoEnum.Comentario.value)
