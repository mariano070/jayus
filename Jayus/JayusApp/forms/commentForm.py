# -*- coding: utf-8 -*-
from django import forms
from JayusApp.models import Contenido, DetalleContenido


class NewCommentForm(forms.ModelForm):
    descripcion = forms.CharField(widget=forms.Textarea, label='Comentario',
                        max_length=250, required=True)

    class Meta:
        model = DetalleContenido
        fields = ('descripcion',)

    def clean_comment(self):
        # Chequea que el comentario no esté vacío
        comentario = self.cleaned_data.get("descripcion")

        if comentario != "":
            raise forms.ValidationError("No puede insertar un comentario vacío.")
        return comentario

