# -*- coding: utf-8 -*-
from JayusApp.models import Miembro, Comunidad
from JayusApp.Common.Enum import RolMiembroEnum


def esMiembro(comunidadParam, userParam):
    return (Miembro.objects.filter(comunidad=comunidadParam.id,
            usuario=userParam.id).count() > 0)


def esLider(comunidadParam, userParam):
    return (Miembro.objects.filter(comunidad=comunidadParam.id,
            usuario=userParam.id).values('rol') == RolMiembroEnum.Lider.value)


def obtener_por_user(userParam):
    return Miembro.objects.filter(usuario=userParam.id).values('comunidad')


def comunidad_por_user(userParam):
    return Comunidad.objects.filter(pk__in=obtener_por_user(userParam))