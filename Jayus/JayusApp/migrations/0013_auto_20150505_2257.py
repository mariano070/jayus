# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0012_customuser_groups'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='groups',
        ),
        migrations.AddField(
            model_name='detallecontenido',
            name='contenido',
            field=models.ForeignKey(default=1, to='JayusApp.Contenido'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='miembro',
            name='rol',
            field=models.ForeignKey(to='auth.Group'),
        ),
    ]
