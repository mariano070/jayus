# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0004_auto_20150430_0410'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contenido',
            name='contenido',
            field=ckeditor.fields.RichTextField(),
        ),
        migrations.AlterField(
            model_name='miembros',
            name='estado',
            field=models.CharField(default=1, max_length=1, choices=[(b'1', b'Activo'), (b'3', b'Baja'), (b'2', b'Bloqueado'), (b'0', b'Pendiente')]),
        ),
    ]
