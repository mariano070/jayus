# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0003_auto_20150422_2109'),
    ]

    operations = [
        migrations.CreateModel(
            name='DetalleContenido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_modificacion', models.DateField(default=django.utils.timezone.now)),
                ('usuario_modificacion', models.CharField(max_length=100, null=True, blank=True)),
                ('descripcion', models.CharField(max_length=250)),
                ('tipo_contenido', models.CharField(default=1, max_length=1, choices=[(b'1', b'Comentario'), (b'3', b'Negativo'), (b'2', b'Positivo')])),
                ('usuario', models.ForeignKey(related_name='usuario_creador_detalle', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DetalleHistoricoContenido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_modificacion', models.DateField(default=django.utils.timezone.now)),
                ('usuario_modificacion', models.CharField(max_length=100, null=True, blank=True)),
                ('descripcion', models.CharField(max_length=250)),
                ('tipo_contenido', models.CharField(default=1, max_length=1, choices=[(b'1', b'Comentario'), (b'3', b'Negativo'), (b'2', b'Positivo')])),
                ('fechaCambio', models.DateField(default=django.utils.timezone.now)),
                ('usuario', models.ForeignKey(related_name='usuario_creador_version', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='contenido',
            name='activo',
        ),
        migrations.RemoveField(
            model_name='miembros',
            name='activo',
        ),
        migrations.RemoveField(
            model_name='miembros',
            name='baneado',
        ),
        migrations.RemoveField(
            model_name='miembros',
            name='rol',
        ),
        migrations.RemoveField(
            model_name='perfil',
            name='Sexo',
        ),
        migrations.RemoveField(
            model_name='registrospendientes',
            name='usuario',
        ),
        migrations.AddField(
            model_name='contenido',
            name='estado',
            field=models.CharField(default=1, max_length=1, choices=[(b'1', b'Activo'), (b'3', b'Editado'), (b'2', b'Eliminado')]),
        ),
        migrations.AddField(
            model_name='contenido',
            name='grupo',
            field=models.ForeignKey(blank=True, to='JayusApp.Grupo', null=True),
        ),
        migrations.AddField(
            model_name='contenido',
            name='usuario_modificacion',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='contenido',
            name='visibilidad',
            field=models.CharField(default=2, max_length=1, choices=[(b'2', b'Privado'), (b'1', b'Publico')]),
        ),
        migrations.AddField(
            model_name='grupo',
            name='nombre',
            field=models.CharField(default=b'Test', max_length=80),
        ),
        migrations.AddField(
            model_name='miembros',
            name='estado',
            field=models.CharField(default=1, max_length=1, choices=[(b'1', b'Activo'), (b'2', b'Bloqueado'), (b'3', b'Eliminado')]),
        ),
        migrations.AddField(
            model_name='miembros',
            name='usuario_modificacion',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='perfil',
            name='imagen',
            field=models.ImageField(default=b'../static/img/users.png', upload_to=b''),
        ),
        migrations.AddField(
            model_name='perfil',
            name='localidad',
            field=models.CharField(max_length=30, blank=True),
        ),
        migrations.AddField(
            model_name='perfil',
            name='pais',
            field=models.CharField(max_length=30, blank=True),
        ),
        migrations.AddField(
            model_name='perfil',
            name='provincia',
            field=models.CharField(max_length=30, blank=True),
        ),
        migrations.AddField(
            model_name='perfil',
            name='sexo',
            field=models.CharField(max_length=1, null=True, choices=[(b'1', b'Femenino'), (b'3', b'Masculino'), (b'2', b'Neutro')]),
        ),
        migrations.AddField(
            model_name='perfil',
            name='telefono',
            field=models.CharField(max_length=30, blank=True),
        ),
        migrations.AddField(
            model_name='perfil',
            name='usuario',
            field=models.OneToOneField(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='registrospendientes',
            name='usuario_modificacion',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='contenido',
            name='contenido',
            field=models.TextField(max_length=500),
        ),
        migrations.AlterField(
            model_name='contenido',
            name='fecha_modificacion',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='contenido',
            name='usuario',
            field=models.ForeignKey(related_name='usuario_creador_contenido', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='grupo',
            name='descripcion',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='miembros',
            name='fecha_modificacion',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='miembros',
            name='grupo',
            field=models.ForeignKey(default=1, to='JayusApp.Grupo'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='miembros',
            name='usuario',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='perfil',
            name='fechaNacimiento',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='registrospendientes',
            name='fecha_generacion',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.DeleteModel(
            name='Roles',
        ),
    ]
