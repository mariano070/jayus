# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('JayusApp', '0013_auto_20150505_2257'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='detallecontenido',
            name='contenido',
        ),
        migrations.AddField(
            model_name='customuser',
            name='groups',
            field=models.ForeignKey(default=1, to='auth.Group'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='miembro',
            name='rol',
            field=models.CharField(default=1, max_length=1, choices=[(b'3', b'Lider'), (b'2', b'Moderador'), (b'1', b'Publicador')]),
        ),
    ]
