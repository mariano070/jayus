# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JayusApp', '0014_auto_20150506_0011'),
    ]

    operations = [
        migrations.AddField(
            model_name='detallecontenido',
            name='contenido',
            field=models.ForeignKey(default=1, to='JayusApp.Contenido'),
            preserve_default=False,
        ),
    ]
