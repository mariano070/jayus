# -*- coding: utf-8 -*-
from django import forms
from JayusApp.models import Contenido, Miembro, Comunidad, Perfil
from django.contrib import admin
from django.contrib.auth.models import Group
#from django.contrib.admin import AdminSite
from ckeditor.widgets import CKEditorWidget
# Consultas
from JayusApp.consultas.consultasComunidad import esMiembro, obtener_por_user
from JayusApp.consultas.consultasComunidad import esLider
# Agregarlo si registramos el user.
from JayusApp.models import CustomUser
from JayusApp.forms.userForm import CustomUserAdmin
# Descomentarlo si mostramos el group.
from django.contrib.auth.models import Group


#class MyAdminSite(AdminSite):
    #logout_template = "logged_out.html"
    #site_header = 'Jayus administration'


class AdminMiembro(admin.TabularInline):
    model = Miembro
    exclude = ['usuario_modificacion', 'fecha_modificacion']
    extra = 1
    min_num = 0

    def save_model(self, request, obj, form, change):
        obj.usuario_modificacion = request.user
        obj.save()


class AdminComunidad(admin.ModelAdmin):
    inlines = [
        AdminMiembro,
    ]
    list_display = ['nombre', 'descripcion']

    # Modificar Queryset
    def get_queryset(self, request):
        qs = super(AdminComunidad, self).get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(pk__in=obtener_por_user(request.user))

    def has_change_permission(self, request, obj=None, **kwargs):
        # Si el objeto es none => true es la lista.
        if (obj is None):
            return True
        else:
            # Si está queriendo editar (Obj lleno)
                    #=> si es el propio si, sino no
            if esMiembro(obj, request.user):
                if esLider(obj, request.user):
                    return True
                return False
            else:
                return False

    # Cuando guarda que lo suba en el rol
    def save_model(self, request, obj, form, change):
        g = Group.objects.get(name='Superuser')
        g.user_set.add(request.user)
        obj.save()



class AdminPerfil(admin.ModelAdmin):
    list_display = ('id', 'imagen', 'nombre', 'apellido',
    'fechaNacimiento', 'sexo', 'localidad', 'provincia', 'pais',
    'telefono')

    exclude = ['usuario']

    def has_add_permission(self, request, **kwargs):
        # No deja editar perfiles
        return False

    def has_change_permission(self, request, obj=None, **kwargs):
        # Si el objeto es none => true es la lista.
        if (obj == None):
            return True
        else:
            # Si está queriendo editar (Obj lleno)
                    #=> si es el propio si, sino no
            if request.user.id == obj.usuario.id:
                return True
            else:
                return False


class AdminContenido(admin.ModelAdmin, forms.ModelForm):
    contenido = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Contenido
        fields = ('usuario', 'titulo')

    list_display = ('titulo', 'contenido', 'visibilidad')
    exclude = ('estado', 'usuario', 'usuario_modificacion',
                'fecha_modificacion')

    def has_add_permission(self, request, **kwargs):
        # No deja editar perfiles
        return False


# Register your models here.
#admin_site = MyAdminSite(name='myadmin')
admin.site.register(Comunidad, AdminComunidad)
admin.site.register(Perfil, AdminPerfil)
admin.site.register(Contenido, AdminContenido)


# Registrar el custom user o no. Según lo que queramos.
admin.site.register(CustomUser, CustomUserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)