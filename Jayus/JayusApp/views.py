# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required
#from django.contrib import admin
from JayusApp.Common.Enum import VisibilidadEnum
from JayusApp.models import RegistrosPendientes, Contenido, DetalleContenido
from JayusApp.models import Perfil, Comunidad, Miembro
from JayusApp.forms.userForm import UserCreationForm, RegistrosPendientesForm
from JayusApp.forms.contenidoForm import ContenidoForm
from JayusApp.forms.commentForm import NewCommentForm
from JayusApp.consultas.consultasComunidad import comunidad_por_user
# Create your views here.


def home(request):
    if not request.user.is_authenticated():
        content_list = Contenido.objects.filter(visibilidad=VisibilidadEnum.Publico.value)
    else:
        content_list = Contenido.objects.all()
    return render(request, 'home.html', {'content_list': content_list})


def gracias(request):
    return render(request, 'gracias.html')


def aboutUs(request):
    return render(request, 'aboutUs.html')


def registrar(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            username = form['username'].value()
            email = form['email'].value()
            usuario = form.save()

            data = {'usuario_modificacion': usuario.id,
                'es_valido': True}
            user_validation = RegistrosPendientesForm(data)
            user_validation.is_valid()
            a_validar = user_validation.save()

            subject = 'Alta usuario'
            message = "Bienvenido a Jayus. \n Gracias por registrarte " + username + ". \n. "
            message += "Para validar, por favor ingrese al siguiente link: "
            message += "http://localhost:8000/validar?id=" + str(a_validar.token)
            sender = 'JayusApp@hotmail.com'
            recipients = [email]

            send_mail(subject, message, sender, recipients, fail_silently=False)
            return HttpResponseRedirect('/gracias')
        else:
            return render(request, 'registrar_user.html', {'form': form})
            #return render(request, 'admin/404.html')
    else:
        if request.user.is_authenticated():
            return HttpResponseRedirect('/home')
        else:
            form = UserCreationForm()
            return render(request, 'registrar_user.html', {'form': form})


def validar(request, id=''):
    if request.method == 'POST':
        if (id != ''):
            user = RegistrosPendientes.objects.filter(token = id).first().usuario_modificacion
            user.is_active = True
            user.is_admin = True
            user.save()

            g = Group.objects.get(name='Novato')
            g.user_set.add(user)

            # Sirve para probar: (Hay que comentar la creación de perfil)
            #http://localhost:8000/validar/?id=1a4753ec-f208-11e4-bbeb-000c29f93b19

            #Crear perfil
            perfil = Perfil()
            perfil.usuario = user
            perfil.save()

            return HttpResponseRedirect('/perfil/' + str(perfil.id))
        else:
            # TODO: Ver es muy fácil sobre escribir
            return render(request, 'admin/404.html')
    else:
        token_recibido = request.GET.get('id', '')
        context = {'id': token_recibido}
        return render(request, 'validar_user.html', context)


@login_required
def perfilView(request, idPerfil=''):
    if (idPerfil != ''):
        query = Perfil.objects.filter(id=idPerfil).first()
        if (query is not None):
            form = {
                'idPerfil': idPerfil,
                'perfil': query,
                'usuario': query.usuario,
                'content_list': Contenido.objects.filter(usuario=query.usuario),
            }
            return render(request, 'perfil.html', form)
        else:
            return render(request, 'admin/404.html')
    else:
        query = Perfil.objects.filter(usuario=request.user.id).first()
        form = {
            'idPerfil': query.id,
            'perfil': query,
            'usuario': request.user,
            'content_list': Contenido.objects.filter(usuario=request.user.id),
        }
        return render(request, 'perfil.html', form)


@login_required
def mis_comunidades(request):
    form = {
        'mis_comunidades': comunidad_por_user(request.user)
    }
    return render(request, 'mis_comunidades.html', form)


@login_required
def comunidad(request, idComunidad):
    if (idComunidad != ''):
        query = Comunidad.objects.filter(id=idComunidad).first()
        if (query is not None):
            if Miembro.objects.filter(comunidad=idComunidad,
                    usuario=request.user.id):
                form = {
                    'idComunidad': idComunidad,
                    'comunidad': query,
                    'content_list': Contenido.objects.filter(comunidad=idComunidad),
                    }
                return render(request, 'comunidad.html', form)

    return render(request, 'admin/404.html')


@login_required
def alta_comentario(request, idContenido):
    if request.method == 'POST':
        if (idContenido is not None):
            form = NewCommentForm(request.POST)
            if form.is_valid():
                obj = DetalleContenido()
                obj.contenido = Contenido.objects.filter(id=idContenido).first()
                obj.descripcion = form['descripcion'].value()
                obj.usuario = request.user
                obj.usuario_modificacion = request.user
                obj.save()
                return HttpResponseRedirect('/contenido/' + str(idContenido))
    return render(request, 'alta_comentario.html', { 'idContenido' : idContenido})


@login_required
def contenido(request, idContenido):
    if (idContenido != 0):
        form = {
            'contenido': Contenido.objects.filter(id=idContenido).first(),
            'formContenido': NewCommentForm,
            'comentarios': DetalleContenido.objects.filter(contenido=idContenido)
            }

        return render(request, 'contenido.html', form)
    return render(request, 'admin/404.html')


# Tipo: Perfil o Comunidad
@login_required
def contenido_add(request, lugar='', idLugar=''):
    form = ContenidoForm()
    if request.method == 'POST':
        form = ContenidoForm(request.POST)
        if form.is_valid():
            obj = Contenido()
            obj.titulo = form['titulo'].value()
            obj.contenido = form['contenido'].value()
            obj.visibilidad = form['visibilidad'].value()
            obj.usuario_modificacion = request.user
            if (lugar == 'perfil'):
                obj.usuario = request.user
                obj.save()
                return HttpResponseRedirect('/perfil')
            else:
                obj.comunidad = Comunidad.objects.filter(id=idLugar).first()
                obj.save()
                return HttpResponseRedirect('/comunidad/' + str(idLugar))
    # Si no se fue ya entonces ver q devuelvo
    if (lugar is not None):
        if (lugar == 'perfil' or idLugar is None):
            obj = Perfil.objects.filter(usuario=request.user.id).first()
            if (obj is None):
                #Crear perfil si no existe
                perfil = Perfil()
                perfil.usuario = request.user
                perfil.save()

            context = {
                'form': form,
                'lugar': lugar,
                'nombre': obj.usuario.username,
                'idLugar': idLugar
                }
            return render(request, 'contenido_add.html', context)

        elif (lugar == 'comunidad' and idLugar is not None):
            obj = Comunidad.objects.filter(id=idLugar).first()
            if (obj is not None):
                context = {
                'form': form,
                'lugar': lugar,
                'nombre': obj.nombre,
                'idLugar': idLugar
                }
            return render(request, 'contenido_add.html', context)
    return render(request, 'admin/404.html')